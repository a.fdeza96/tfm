#!/bin/tcsh
set mem='450'
set core='60'
set ori_dir=`pwd`
set workdir = `mktemp -u /junk/$USER/cellranger.XXXX`
mkdir -p $workdir
cd $workdir
(/ngs/software/cellranger/latest/cellranger count --id=Elyahu_O1 --sample=O1 --fastqs=/ngs/afernandez/data/elyahu/HVTMHBGX2/O1/,/ngs/afernandez/data/elyahu/HVV3FBGX2/O1/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core > $ori_dir/O1_O2.log) >& $ori_dir/O1_O2.err
mv Elyahu_O1 $ori_dir/
#/ngs/software/cellranger/latest/cellranger count --id=Elyahu_O1_2 --sample=O1 --fastqs=/ngs/afernandez/data/elyahu/HVV3FBGX2/O1/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
#mv Elyahu_O1_2 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Elyahu_O2 --sample=O2 --fastqs=/ngs/afernandez/data/elyahu/HVTMHBGX2/O2/,/ngs/afernandez/data/elyahu/HVV3FBGX2/O2/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Elyahu_O2 $ori_dir/
#/ngs/software/cellranger/latest/cellranger count --id=Elyahu_O2_2 --sample=O2 --fastqs=/ngs/afernandez/data/elyahu/HVV3FBGX2/O2/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
#mv Elyahu_O2_2 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Elyahu_O3 --sample=O3 --fastqs=/ngs/afernandez/data/elyahu/H7JH2BGX3/O3/,/ngs/afernandez/data/elyahu/H7JTCBGX3/O3/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Elyahu_O3 $ori_dir/
#/ngs/software/cellranger/latest/cellranger count --id=Elyahu_O3_2 --sample=O3 --fastqs=/ngs/afernandez/data/elyahu/H7JTCBGX3/O3/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
#mv Elyahu_O3_2 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Elyahu_O4 --sample=O4 --fastqs=/ngs/afernandez/data/elyahu/H7JH2BGX3/O4/,/ngs/afernandez/data/elyahu/H7JTCBGX3/O4/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Elyahu_O4 $ori_dir/
#/ngs/software/cellranger/latest/cellranger count --id=Elyahu_O4_2 --sample=O4 --fastqs=/ngs/afernandez/data/elyahu/H7JTCBGX3/O4/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
#mv Elyahu_O4_2 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Elyahu_Y1 --sample=Y1 --fastqs=/ngs/afernandez/data/elyahu/HVTMHBGX2/Y1/,/ngs/afernandez/data/elyahu/HVV3FBGX2/Y1/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Elyahu_Y1 $ori_dir/
#/ngs/software/cellranger/latest/cellranger count --id=Elyahu_Y1_2 --sample=Y1 --fastqs=/ngs/afernandez/data/elyahu/HVV3FBGX2/Y1/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
#mv Elyahu_Y1_2 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Elyahu_Y2 --sample=Y2 --fastqs=/ngs/afernandez/data/elyahu/HVTMHBGX2/Y2/,/ngs/afernandez/data/elyahu/HVV3FBGX2/Y2/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Elyahu_Y2 $ori_dir/
#/ngs/software/cellranger/latest/cellranger count --id=Elyahu_Y2_2 --sample=Y2 --fastqs=/ngs/afernandez/data/elyahu/HVV3FBGX2/Y2/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
#mv Elyahu_Y2_2 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Elyahu_Y3 --sample=Y3 --fastqs=/ngs/afernandez/data/elyahu/H7JH2BGX3/Y3/,/ngs/afernandez/data/elyahu/H7JTCBGX3/Y3/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Elyahu_Y3 $ori_dir/
#/ngs/software/cellranger/latest/cellranger count --id=Elyahu_Y3_2 --sample=Y3 --fastqs=/ngs/afernandez/data/elyahu/H7JTCBGX3/Y3/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
#mv Elyahu_Y3_2 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Elyahu_Y4 --sample=Y4 --fastqs=/ngs/afernandez/data/elyahu/H7JH2BGX3/Y4/,/ngs/afernandez/data/elyahu/H7JTCBGX3/Y4/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Elyahu_Y4 $ori_dir/
#/ngs/software/cellranger/latest/cellranger count --id=Elyahu_Y4_2 --sample=Y4 --fastqs=/ngs/afernandez/data/elyahu/H7JTCBGX3/Y4/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
#mv Elyahu_Y4_2 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Sample28 --sample=Sample28_scRNASeq --fastqs=/ngs/afernandez/data/lab/GSoto_SC_RNA_Seq_10XGenomics/Sample28_scRNASeq/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Sample28 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Sample29 --sample=Sample29_scRNASeq --fastqs=/ngs/afernandez/data/lab/GSoto_SC_RNA_Seq_10XGenomics/Sample29_scRNASeq/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Sample29 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Sample30 --sample=Sample30_scRNASeq --fastqs=/ngs/afernandez/data/lab/GSoto_SC_RNA_Seq_10XGenomics/Sample30_scRNASeq/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Sample30 $ori_dir/
/ngs/software/cellranger/latest/cellranger count --id=Sample50 --sample=Sample50_scRNASeq --fastqs=/ngs/afernandez/data/lab/GSoto_SC_RNA_Seq_10XGenomics/Sample50_scRNASeq/ --transcriptome=/ngs/software/cellranger/refdata-gex-mm10-2020-A/ --localmem=$mem --localcores=$core
mv Sample50 $ori_dir/



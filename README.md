Autor: Álvaro Fernández Almeida

Este repositorio ha sido creado como material adicional en el trabajo de fin de máster en el máster en bioinformática aplicada a la medicina personalizada y la salud.
Todos los scripts aqui presentes han sido utilizados para la creación del trabajo. Algunos scripts tambien utilizados en el trabajo no se han incluido en este repositorio debido a que eran variaciones de los originales o los datos que aportan no estan presentes en la presentación final del trabajo.

Los scripts numerados en la carpeta Seurat son la guia principal usada para el procesamiento de los datos con el programa.

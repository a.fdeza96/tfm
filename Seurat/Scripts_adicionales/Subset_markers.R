#################################################
#### Script para encontrar los genes diferencialmente expresados por condición en el conjunto de poblaciones o una determinada

##Clear all variables in the enviroment

rm(list=ls())

## Librerias

library(fitdistrplus)
library(mgcv)
library(spatstat.core)
library(cluster)
library(Seurat) 
library(RColorBrewer)
library(ggplot2) 
library(dplyr)
library(data.table)
library(grid)

##seed

set.seed(109)

## Seleccionar el directorio

setwd("/ngs/afernandez/out/seurat/Elyahu/")

## Cargar los datos 

seurat_integrated <- readRDS("output/integrated/Mix_8samples_downsample_original_integrate_non_normalize_clusters_leiden_subcluster.rds")
load("annotations_Mm.RData") # Anotaciones de los genes

# Para obtener la lista completa de genes, se utilizan todos los datos normalizados, no la lista de 3000 genes seleccionados previamente 

Idents(seurat_integrated)
DefaultAssay(seurat_integrated)<-"RNA"
seurat_integrated <- NormalizeData(seurat_integrated)
all.genes <- rownames(seurat_integrated)
seurat_integrated <- ScaleData(seurat_integrated, features = all.genes)

Idents(seurat_integrated) <- "subset"
levels(seurat_integrated)
all.markers <- FindAllMarkers(seurat_integrated)
all.markers %>%
  group_by(cluster) %>%
  slice_max(n = 2, order_by = avg_log2FC)

# Se añaden anotaciones a la lista de genes obtenida

all_ann_markers <- all.markers %>% 
  rownames_to_column(var="gene") %>% 
  left_join(y = unique(annotations[, c("gene_name", "description")]),
            by = c("gene" = "gene_name"))

# Guardar la lista de genes

write.csv(all_ann_markers,file = "csv/mix/allmarkers2610_no_filter_algorithm4_subsets.csv", row.names = TRUE)
print("csv saved!")


##### Comparativa condiciones de una población especifica

seurat_integrated$clust_cond <- paste(Idents(seurat_integrated),seurat_integrated$subset,sep = "_")

combined_markers <- FindMarkers(seurat_integrated, ident.1 = "Treg_Klrg1", ident.2 = "aTreg")
write.csv(combined_markers,file = "csv/mix/Klrg1_vs_aTreg.csv", row.names = TRUE)
print("csv saved!")
combined_markers <- FindMarkers(seurat_integrated, ident.1 = "Treg_Klrg1", ident.2 = "Treg")
write.csv(combined_markers,file = "csv/mix/Klrg1_vs_Treg.csv", row.names = TRUE)
print("csv saved!")
combined_markers <- FindMarkers(seurat_integrated, ident.1 = "Treg_Klrg1", ident.2 = c("aTreg","Treg"))
write.csv(combined_markers,file = "csv/mix/Klrg1_vs_aTreg_&_Treg.csv", row.names = TRUE)
print("csv saved!")
combined_markers <- FindMarkers(seurat_integrated, ident.1 = "Treg_Klrg1")
write.csv(combined_markers,file = "csv/mix/Klrg1_vs_all.csv", row.names = TRUE)
print("csv saved!")


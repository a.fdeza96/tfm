#################################################
#### Script para hacer piecharts y umap a las condiciones y a todas las muestras

##Clear all variables in the enviroment

rm(list=ls())

## Librerias

library(fitdistrplus)
library(mgcv)
library(spatstat.core)
library(cluster)
library(Seurat) 
library(RColorBrewer)
library(ggplot2) 
library(dplyr)
library(data.table)
library(grid)

##seed

set.seed(109)

## Seleccionar el directorio

setwd("/ngs/afernandez/out/seurat/Elyahu/")

## Cargar los datos 

seurat_integrated <- readRDS("output/integrated/Mix_8samples_downsample_original_integrate_non_normalize_clusters_leiden_subcluster.rds")

levels(merged_seurat) <- c("Naive","Isg_15","Treg","aTreg","Treg_Klrg1","Exhausted","TEM","TEM_Crxr6","Cytotoxic")
color <- c("#7fcdbb","#2b8cbe","#a6bddb","#c51b8a","#e0d004","#31a354","#d95f0e","#636363","#ff3737")
##create the subsets
seurat_integrated_Young <- subset(merged_seurat, subset = sample == "Y1_Y2" | sample == "Y3_Y4")
seurat_integrated_Old <- subset(merged_seurat, subset = sample == "O1_O2" | sample == "O3_O4")
seurat_integrated_Knockout <- subset(merged_seurat, subset = sample == "Sample28" | sample == "Sample30" | sample == "Sample50")
seurat_integrated_control <- subset(merged_seurat, subset = sample == "Sample29")
remove("merged_seurat")
remove("seurat_integrated_control")
gc()
#umaps cluster por subset, add paleta color
pdf("Cluster_per_sample_1310.pdf")
DimPlot(seurat_integrated_Young,
        reduction = "umap",
        label = F,
        label.size = 6, cols = "Set1") + plot_annotation(title = "Young")
DimPlot(seurat_integrated_Old,
        reduction = "umap",
        label = F,
        label.size = 6, cols = "Set1") + plot_annotation(title = "Old")
DimPlot(seurat_integrated_Knockout,
        reduction = "umap",
        label = F,
        label.size = 6, cols = "Set1") + plot_annotation(title = "Knockout")
DimPlot(seurat_integrated_control,
        reduction = "umap",
        label = F,
        label.size = 6, cols = color ) + plot_annotation(title = "Control") # enviado a maria c("#d95f0e","#feb24c","#ff3737","#c51b8a","#e0d004","#636363","#2ca25f","#2b8cbe","#7fcdbb")c("#d95f0e","#feb24c","#ff3737","#c51b8a","#e0d004","#636363","#2ca25f","#2b8cbe","#7fcdbb")
dev.off()
##Save the subsets
saveRDS(merged_seurat,"output/integrated/Mix_8samples_downsample_original_integrate_non_normalize_clusters_leiden_subcluster.rds")
saveRDS(seurat_integrated_Young,"output/subset_young_leiden_1310.rds")
saveRDS(seurat_integrated_Old,"output/subset_old_leiden_1310.rds")
saveRDS(seurat_integrated_Knockout,"output/subset_knock_leiden_1310.rds")
saveRDS(seurat_integrated_control,"output/subset_control_leiden_1310.rds")
seurat_integrated_Young <-readRDS("output/subset_young_leiden_1310.rds")
seurat_integrated_Old <-readRDS("output/subset_old_leiden_1310.rds")
seurat_integrated_Knockout <-readRDS("output/subset_knock_leiden_1310.rds")
seurat_integrated_control <-readRDS("output/subset_control_leiden_1310.rds")
levels(merged_seurat)<- c("Naive","Isg_15","Treg","aTreg","Treg_Klrg1","Exhausted","TEM","TEM_Crxr6","Cytotoxic")
levels(seurat_integrated_Young)<- c("Naive","Isg_15","Treg","aTreg","Treg_Klrg1","Exhausted","TEM","TEM_Crxr6","Cytotoxic")
levels(seurat_integrated_Old)<- c("Naive","Isg_15","Treg","aTreg","Treg_Klrg1","Exhausted","TEM","TEM_Crxr6","Cytotoxic")
levels(seurat_integrated_Knockout)<- c("Naive","Isg_15","Treg","aTreg","Treg_Klrg1","Exhausted","TEM","TEM_Crxr6","Cytotoxic")
levels(seurat_integrated_control)<- c("Naive","Isg_15","Treg","aTreg","Treg_Klrg1","Exhausted","TEM","TEM_Crxr6","Cytotoxic")
merged_seurat@reductions$umap@cell.embeddings[,1] <- merged_seurat@reductions$umap@cell.embeddings[,1] *-1
seurat_integrated_Young@reductions$umap@cell.embeddings[,1] <- seurat_integrated_Young@reductions$umap@cell.embeddings[,1] *-1
seurat_integrated_Old@reductions$umap@cell.embeddings[,1] <- seurat_integrated_Old@reductions$umap@cell.embeddings[,1] *-1
seurat_integrated_Knockout@reductions$umap@cell.embeddings[,1] <- seurat_integrated_Knockout@reductions$umap@cell.embeddings[,1] *-1
seurat_integrated_control@reductions$umap@cell.embeddings[,1] <- seurat_integrated_control@reductions$umap@cell.embeddings[,1] *-1
color <- c("#7fcdbb","#2b8cbe","#a6bddb","#c51b8a","#e0d004","#31a354","#d95f0e","#636363","#ff3737")
#create the percentages to the piecharts
n_cells_integrated <- FetchData(merged_seurat, 
                                vars = c("ident", "sample")) %>%
  dplyr::count(ident, sample) %>%
  tidyr::spread(ident, n)
#n_cells[c(2,4),c(7)] <- 0
n_cells_integrated[is.na(n_cells_integrated)]<-0
n_cells_integrated$total <- rowSums(n_cells_integrated[2:length(n_cells_integrated)])
n_cells_integrated[nrow(n_cells_integrated)+1,2:length(n_cells_integrated)] <- colSums(n_cells_integrated[1:nrow(n_cells_integrated),2:length(n_cells_integrated)])
percent_cells <- n_cells_integrated
for( i in 2:length(percent_cells)){
  percent_cells[,i] <- percent_cells[,i]/percent_cells[,length(percent_cells)]
}
percent_cells[2:length(percent_cells)] <- percent_cells[2:length(percent_cells)]*100
percent_cells$total_cells <- n_cells_integrated$total
percent_cells[2:length(percent_cells)] <- round(percent_cells[2:length(percent_cells)], digits = 2)
##number cells per cluster and percent
n_cells_integrated <- FetchData(seurat_integrated_Young, 
                     vars = c("ident", "sample")) %>%
  dplyr::count(ident, sample) %>%
  tidyr::spread(ident, n)
n_cells_integrated[is.na(n_cells_integrated)]<-0
n_cells_integrated$total <- rowSums(n_cells_integrated[2:length(n_cells_integrated)])
n_cells_integrated[nrow(n_cells_integrated)+1,2:length(n_cells_integrated)] <- colSums(n_cells_integrated[1:nrow(n_cells_integrated),2:length(n_cells_integrated)])
percent_cells_young <- n_cells_integrated
for( i in 2:length(percent_cells_young)){
  percent_cells_young[,i] <- percent_cells_young[,i]/percent_cells_young[,length(percent_cells_young)]
}
percent_cells_young[2:length(percent_cells_young)] <- percent_cells_young[2:length(percent_cells_young)]*100
percent_cells_young$total_cells <- n_cells_integrated$total
percent_cells_young[2:length(percent_cells_young)] <- round(percent_cells_young[2:length(percent_cells_young)], digits = 2)
##number cells per cluster and percent
n_cells_integrated <- FetchData(seurat_integrated_Old, 
                     vars = c("ident", "sample")) %>%
  dplyr::count(ident, sample) %>%
  tidyr::spread(ident, n)
n_cells_integrated[is.na(n_cells_integrated)]<-0
n_cells_integrated$total <- rowSums(n_cells_integrated[2:length(n_cells_integrated)])
n_cells_integrated[nrow(n_cells_integrated)+1,2:length(n_cells_integrated)] <- colSums(n_cells_integrated[1:nrow(n_cells_integrated),2:length(n_cells_integrated)])
percent_cells_old <- n_cells_integrated
for( i in 2:length(percent_cells_old)){
  percent_cells_old[,i] <- percent_cells_old[,i]/percent_cells_old[,length(percent_cells_old)]
}
percent_cells_old[2:length(percent_cells_old)] <- percent_cells_old[2:length(percent_cells_old)]*100
percent_cells_old$total_cells <- n_cells_integrated$total
percent_cells_old[2:length(percent_cells_old)] <- round(percent_cells_old[2:length(percent_cells_old)], digits = 2)
##number cells per cluster and percent
n_cells_integrated <- FetchData(seurat_integrated_Knockout, 
                     vars = c("ident", "sample")) %>%
  dplyr::count(ident, sample) %>%
  tidyr::spread(ident, n)
n_cells_integrated[is.na(n_cells_integrated)]<-0
n_cells_integrated$total <- rowSums(n_cells_integrated[2:length(n_cells_integrated)])
n_cells_integrated[nrow(n_cells_integrated)+1,2:length(n_cells_integrated)] <- colSums(n_cells_integrated[1:nrow(n_cells_integrated),2:length(n_cells_integrated)])
percent_cells_knockout <- n_cells_integrated
for( i in 2:length(percent_cells_knockout)){
  percent_cells_knockout[,i] <- percent_cells_knockout[,i]/percent_cells_knockout[,length(percent_cells_knockout)]
}
percent_cells_knockout[2:length(percent_cells_knockout)] <- percent_cells_knockout[2:length(percent_cells_knockout)]*100
percent_cells_knockout$total_cells <- n_cells_integrated$total
percent_cells_knockout[2:length(percent_cells_knockout)] <- round(percent_cells_knockout[2:length(percent_cells_knockout)], digits = 2)
##number cells per cluster and percent
n_cells_integrated <- FetchData(seurat_integrated_control, 
                     vars = c("ident", "sample")) %>%
  dplyr::count(ident, sample) %>%
  tidyr::spread(ident, n)
n_cells_integrated[is.na(n_cells_integrated)]<-0
n_cells_integrated$total <- rowSums(n_cells_integrated[2:length(n_cells_integrated)])
n_cells_integrated[nrow(n_cells_integrated)+1,2:length(n_cells_integrated)] <- colSums(n_cells_integrated[1:nrow(n_cells_integrated),2:length(n_cells_integrated)])
percent_cells_control <- n_cells_integrated
for( i in 2:length(percent_cells_control)){
  percent_cells_control[,i] <- percent_cells_control[,i]/percent_cells_control[,length(percent_cells_control)]
}
percent_cells_control[2:length(percent_cells_control)] <- percent_cells_control[2:length(percent_cells_control)]*100
percent_cells_control$total_cells <- n_cells_integrated$total
percent_cells_control[2:length(percent_cells_control)] <- round(percent_cells_control[2:length(percent_cells_control)], digits = 2)
##piecharts
pdf("Piechart_&_Umap_Cluster_per_sample_3110.pdf")
d<-DimPlot(merged_seurat,
        reduction = "umap",
        label = F,
        label.size = 6, cols = color) + plot_annotation(title = "All")
d1<-DimPlot(seurat_integrated_Young,
        reduction = "umap",
        label = F,
        label.size = 6, cols = color) + plot_annotation(title = "Young")
d2<-DimPlot(seurat_integrated_Old,
        reduction = "umap",
        label = F,
        label.size = 6, cols = color) + plot_annotation(title = "Old")
d3<-DimPlot(seurat_integrated_Knockout,
        reduction = "umap",
        label = F,
        label.size = 6, cols = color) + plot_annotation(title = "Knockout")
d4<-DimPlot(seurat_integrated_control,
        reduction = "umap",
        label = F,
        label.size = 6, cols = color) + plot_annotation(title = "Control")
d
d1
d2
d3
d4
pie(as.integer(percent_cells[9,2:10]*100), labels = paste(), col = color, main = "All", border = "white")
legend("bottomleft",paste(colnames(percent_cells[,2:10])," (", percent_cells[9,2:10],"%)", sep=""), col = color, pch = 15, text.width = 0.75, cex = 0.5,box.lty=0)
pie(as.integer(percent_cells_young[3,2:10]*100), labels = paste(), col = color, main = "Young", border = "white")
legend("bottomleft",paste(colnames(percent_cells_young[,2:10])," (", percent_cells_young[3,2:10],"%)", sep=""), col = color, pch = 15, text.width = 0.75, cex = 0.5,box.lty=0)
pie(as.integer(percent_cells_old[3,2:10]*100), labels = paste(), col = color, main = "Old", border = "white")
legend("bottomleft",paste(colnames(percent_cells_old[,2:10])," (", percent_cells_old[3,2:10],"%)", sep=""), col = color, pch = 15, text.width = 0.75, cex = 0.5,box.lty=0)
pie(as.integer(percent_cells_knockout[3,2:10]*100), labels = "", col = color, main = "Knockout", border = "white")
legend("bottomleft",paste(colnames(percent_cells_knockout[,2:10])," (", percent_cells_knockout[3,2:10],"%)", sep=""), col = color, pch = 15, text.width = 0.75, cex = 0.5,box.lty=0)
pie(as.integer(percent_cells_control[2,2:10]*100), labels = paste(), col = color, main = "Control", border = "white")
legend("bottomleft",paste(colnames(percent_cells_control[,2:10])," (", percent_cells_control[2,2:10],"%)", sep=""), col = color, pch = 15, text.width = 0.75, cex = 0.5,box.lty=0)
dev.off()
